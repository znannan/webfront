import React,{ Component } from 'react';

class ToDoItem extends Component{
    changeHandler(event){
        //console.log(event.target.checked)
        this.props.changeHandler();
    }
    deleteHandler(event){
        //console.log(event.target.id)
        this.props.deleteHandler();
    }
    render(){
        return(
            <li>
                <label className={this.props.todo.done?"done":"undo"}>
                    <input type="checkbox" defaultChecked={this.props.todo.done?true:false} onChange={this.changeHandler.bind(this)} />
                    {this.props.todo.content}
                </label>
                <span title="delete" onClick={this.deleteHandler.bind(this)}>X</span>
            </li>
        )
    }

}

export default ToDoItem;