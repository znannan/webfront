import React, { Component } from 'react';
import '../css/App.scss';
import ToDoList from './ToDoList';
import NewToDoForm from './NewToDoForm';
import todos from '../todos.json';

let lastIndex = todos[todos.length - 1].id ;
class App extends Component {
  constructor(props){
    super(props);
    

    this.state = {
      todos: todos
    }
  }
  createNewHandler(content){
    //console.log(content);
    const todo = this.generateTodo(content);
    this.setState((state, props) => {
      return {
        todos: state.todos.concat(todo)
      }
    })
  }
  changeHandler(id, event){
    this.setState(
      (state,props) => {
        const newList = state.todos.map(
          e => {
            if (e.id === id){
              e.done = !e.done
            }
            return e;
          }
        )
        return {todos:newList}
      }
    )
  }
  deleteHandler(id, event){
    this.setState(
      (state,props) => {
        const aconfirm = window.confirm("Are you sure?");
        if(aconfirm){
          const newList = this.deleteById(state.todos, id)
          return{
            todos: newList
          }
        }
      }
    )
  }
  deleteById(todo, id){
    const newList = todo.filter(e => e.id !== id);
    return newList
  }
  generateTodo(content){
    const id = lastIndex + 1;
    lastIndex = id;
    return {
      id: lastIndex + 1,
      content: content,
      done: false
    }

  }
  render() {
    return (
      <div id="wrap">
        <h1>My ToDo List</h1>
        <ToDoList todos={this.state.todos} changeHandler={this.changeHandler.bind(this)} deleteHandler={this.deleteHandler.bind(this)}/>
        <NewToDoForm createNewHandler={this.createNewHandler.bind(this)} />
      </div>
    );
  }
}

export default App;
