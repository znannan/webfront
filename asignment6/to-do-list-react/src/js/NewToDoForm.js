import React, { Component } from 'react';

class NewToDoForm extends Component{

    constructor(props){
        super(props);
        this.state = {
            content: ''
        }
        

    }
    formSubmitHandler(event){
        event.preventDefault();
        const content = this.state.content.trim();
        if (content && content.length>0){
            this.props.createNewHandler(content);
            this.setState({
                content: ''
            })
        }

    }
    contentChangeHandler(event){
        //console.log(event.target.value)
        this.setState({
            content: event.target.value
        })
    }
    render(){
        return(
            <div>
                <form id="new-todo-form" onSubmit={this.formSubmitHandler.bind(this)}>
                    <textarea value={this.state.content} onChange={this.contentChangeHandler.bind(this)} rows="7" cols="70" id="new_todo"></textarea>
                    <div id="submit_div"><input type="submit" id="submit_button" /></div>
                </form>
            </div>
        )
    }
}

export default NewToDoForm;