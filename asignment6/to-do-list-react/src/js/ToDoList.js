import React,{ Component } from 'react';
import ToDoItem from './ToDoItem';
import todos from '../todos.json';

class ToDoList extends Component{
    constructor(props){
        super(props);
    }

    get todos(){
        return this.props.todos.map(
            (todo) => <ToDoItem todo={todo} key={todo.id} changeHandler = {this.props.changeHandler.bind({},todo.id) } deleteHandler = {this.props.deleteHandler.bind({},todo.id)} />
        )
    }
    
    render(){
        return(
            <ul id="todolist_ul">
                {this.todos}
            </ul>
        )
    }

}

export default ToDoList;