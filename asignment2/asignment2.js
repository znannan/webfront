/*
*1. write a function to calculate factorial using for loop. 
*factorial: the product of an integer and all the integers below i.t; for example factorial four ( 4! ) is equal to 24.
*
*function name: factorial
*params: n, type: number
*return: number
*e.g. factorial(4) should return 24 which is the result of 1*2*3*4
*/
function factorial(n){
    if(typeof(n)!='number'){throw("Input must be a number");  }
    var rs=1;
    for(var i=1;i<=n;i++){
        rs = rs*i;
    }
    return rs;
}
console.log(factorial(4));
//console.log(factorial('hello'));
/*
*2. write a function to get the extension of a filename.
*if the filename has more than one extension, return the last one.
*
*function name: ext
*params: filename, type: string
*return: string
*e.g. 
*filename('abc.txt') should return 'txt'
*filename('abc.tar.gz') should return 'gz'
*filename('index.html.erb') should return 'erb'
*/
function filename_ext(filename){
    if(typeof(filename)!='string'){throw("Input must be a string")  }
    var ext;
    ext = filename.split(".").pop();
    return ext;
}
console.log(filename_ext('index.html.erb'));

/*
*3. write a function to test if a given year is a leap year.
*According the wikipedia, a leap year is:
*Every year that is exactly divisible by four is a leap year, except for years that are exactly divisible by 100, but these centurial years are leap years if they are exactly divisible by 400. For example, the years 1700, 1800, and 1900 are not leap years, but the year 2000 is.
*
*function name: isLeapYear
*params: year, type: number
*return: boolean
*e.g.
*isLeapYear(1996) // true
*isLeapYear(1997) // false
*isLeapYear(1900) // false
*isLeapYear(2000) // true
*/

function isLeapYear(year){
    if(typeof(year)!='number'){throw("Input year must be a number")  }
    if(year%400 == 0){
        return true;
    }else if(year%100 == 0){
        return false;
    }else if(year%4 == 0){
        return true;
    }else{
        return false;
    }
}
console.log(isLeapYear(1996));
console.log(isLeapYear(1997));
console.log(isLeapYear(1900));
console.log(isLeapYear(2000));

/*
*4. given an array of numbers, write a function to select all the even numbers, and then double each element, finally return the sum.
*for example, given [1,2,3,4,5,6,7,8,9], filter even numbers we get [2,4,6,8], and double each filtered element we get [4,8,12,16], finally sum all the doubled even number, we get 40.
*
*function name: evenDoubleSum
*params: arr, type: array
*return: number
*e.g.
*evenDoubleSum([1,2,3,4,5,6,7,8,9]) // 40
*evenDoubleSum([1,3,5,7,9]) // 0
*evenDoubleSum([2,4]) // 12
 */
function evenDoubleSum(arr){
    if(typeof(arr)!='object'){throw("Input arr must be a array")  }
    var sum = 0;
    arr.map(val=>(val%2==0?sum+=val:sum))
    sum = sum*2
    return sum
}
console.log(evenDoubleSum([1,2,3,4,5,6,7,8,9]));
console.log(evenDoubleSum([1,3,5,7,9]));
console.log(evenDoubleSum([2,4]));
//console.log(evenDoubleSum('hello'));
/*
*5. write a function to test if a string is a valid email format using regular expression.
*a valid email format: [username]@[domain name], the username must 
*start with a alphabet or number, and contains at least one character. 
*the domain name should be [characters].[characters] 
*
*function name: isEmail
*params: str, type: string
*return: boolean
*e.g. isEmail('123') //false
*isEmail('@abc.com') //false
*isEmail('abc@123') //false
*isEmail('abc@gmail.com') //true
*isEmail('123456@qq.com') //true 
*/
function isEmail(str){
    if(typeof(str)!='string'){throw("Input str must be a string")  }
    var reg = /^[a-z0-9]+@([a-z0-9])+\.([a-z0-9])/;
    return reg.test(str);
}
console.log(isEmail('123'));
console.log(isEmail('@abc.com'));
console.log(isEmail('abc@123'));
console.log(isEmail('abc@gmail.com'));
console.log(isEmail('123456@qq.com'));
//console.log(isEmail(123));
