import React, { Component } from 'react';
import BookNavItem from './BookNavItem';
import { Menu } from 'antd';

const SubMenu = Menu.SubMenu;

class BookNav extends Component{
    constructor(props){
        super(props);
    }

    navItems(items){
        return items.map(ele=>{
            return(
                <Menu.Item key={ele.SN} onClick={this.props.changeCurrentBook.bind({}, ele)}>
                    <span>{ele.FullName}</span>
                </Menu.Item>
            )
        })
    }

    get oldTestments(){
        return this.props.books.slice(0,38);
    }

    get newTestments(){
        return this.props.books.slice(39)
    }


    render(){
        return(
            <Menu theme="dark" mode='inline' defaultSelectedKeys={['1']}
              defaultOpenKeys={['Old','New']}>
                <SubMenu key="Old" title={<span>旧约</span>}>
                    {this.navItems(this.oldTestments)}
                </SubMenu>
                <SubMenu key="New" title={<span>新约</span>}>
                    {this.navItems(this.newTestments)}
                </SubMenu>  
            </Menu>
           /**
            *  <nav className="book-nav">
                <ul className="book-list">
                    {this.navItems}
                </ul>
            </nav>
            */
        )
    }
}
export default BookNav;