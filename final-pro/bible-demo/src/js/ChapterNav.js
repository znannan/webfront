import React, { Component } from 'react';
import { Pagination } from 'antd';

class ChapterNav extends Component{
    changeChapter(chapter,event){
        //console.log(chapter);
        this.props.changeChapter(chapter);
    }

    get navItems(){
        if (this.props.chapters){
            let chapters = [];
            for (let i = 1; i <= this.props.chapters; i++){
                let nav = <li key={i} className={this.props.currentChapter===i?'':''} onClick={this.changeChapter.bind(this,i)}>{i}</li>;
                chapters.push(nav);
            }
            return chapters;
        }else{
            return [];
        }
    }

    renderChapter(current,type,originalElement){
        if(type === 'prev'){
            return ''
        }
        if(type === 'next'){
            return ''
        }
        return originalElement;
    }

    render(){
        return(
            // <ul className="chapter-nav">
            //     {this.navItems}
            // </ul>
            <Pagination 
                total={this.props.chapters}
                defaultPageSize={1}
                onChange={this.props.changeChapter} 
                itemRender={this.renderChapter} 
                current={this.props.currentChapter}
            />
        )
    }
}
export default ChapterNav;