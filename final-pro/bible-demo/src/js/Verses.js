import React, { Component } from 'react';
import { List,Typography } from 'antd';
class Verses extends Component{
    constructor(props){
      super(props);
      this.state = {
        verses:[]
      }
    }

    componentDidUpdate(prevProps){
      // console.log("componentDidUpdate");
      // console.log(prevProps);
      if(this.shouldFetch(prevProps)){
        this.fetchVerses();

      }
    }

    shouldFetch(prevProps){
      return  this.props.currentBook && 
              this.props.currentChapter && 
              (this.bookId(prevProps.currentBook) !== this.bookId(this.props.currentBook) || prevProps.currentChapter !== this.props.currentChapter)
    }

    bookId(book){
      return book && book.SN
    }

    fetchVerses(){
      fetch(this.url).then(res=>{
        return res.json()
      }).then(verses => {
        // console.log(verses);
        this.setState({
          verses:verses
        })
      }).catch(err => {
        //console.log(err);
      })
    }

    get verseItems(){
      return this.state.verses.map(verse => {
        return (
          <li key={verse.VerseSN}>
            <label>{verse.VerseSN}</label>
            <p>{verse.Lection}</p>
          </li>
        )

      })
    }

    renderVerse(verse){
      return(
        <List.Item key={verse.ID}>
          <Typography.Text>{verse.VerseSN}</Typography.Text>{verse.Lection}
        </List.Item>
      )
    }

    get url(){
      return `https://immense-dawn-54030.herokuapp.com/bible/${this.props.currentBook.SN}/${this.props.currentChapter}`
    }

    render(){
        return(
            // <ul>
            //     {this.verseItems}
            // </ul>
            <List bordered dataSource={this.state.verses} renderItem={this.renderVerse} />
        )
    }
}
export default Verses;