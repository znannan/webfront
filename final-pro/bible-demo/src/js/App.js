import React, { Component } from 'react';
import '../css/App.css';
import BookNav from './BookNav';
import Content from './Content';
import "antd/dist/antd.css";
import zhCN from 'antd/lib/locale-provider/zh_CN';
import { Layout, Icon, Typography } from 'antd';
const { Header, Sider, Content: Main} = Layout;


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      books: [],
      currentBook: null,
      collapsed: false,
      contentMarginLeft:200,
    }
  }
  componentDidMount(){
    fetch('https://immense-dawn-54030.herokuapp.com/bible').then(res=>{
      return res.json()
    }).then(books => {
      //console.log(books);
      this.setState({
        books:books,
        currentBook:books[0]
      })
    }).catch(err => {
      console.log(err);
    })
  }

  changeCurrentBook(book){
    this.setState({
      currentBook: book
    })

  }

  toggle(){
      this.setState((prevState)=>{
        let contentMarginLeft = 200;
        if(!prevState.collapsed){
          contentMarginLeft = 0;
        }
        return {
          collapsed: !prevState.collapsed,
          contentMarginLeft: contentMarginLeft
        }
      })
  }

  render() {
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}
          collapsedWidth={0}
          style={{ overflow:'auto', height:'100vh', position:'fixed', left:0}}
        >
          <BookNav books={this.state.books} changeCurrentBook={this.changeCurrentBook.bind(this)} />
        </Sider>
        <Layout style={{ marginLeft:this.state.contentMarginLeft, transitionProperty:'marginLeft', transitionDuration:1 }}>
          <Header style={{ background: '#fff', padding:0 }}>
            <Icon className='trigger' type={this.state.collapsed?'menu-unfold':'menu-fold'} onClick={this.toggle.bind(this)} />
            <span style={{fontSize:24, textAlign:"center"}}>中文圣经简体和合本</span>
          </Header>
          <Main style={{margin:'24px 16px', padding:24, background:'#fff', minHeight:100}} >
            <Content currentBook={this.state.currentBook} />
          </Main>
        </Layout>
      </Layout>
      
    );
  }
}

export default App;
