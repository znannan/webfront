import React, { Component } from 'react';
class BookNavItem extends Component{
    render(){
        return(
            <li onClick={this.props.changeCurrentBook.bind({}, this.props.book)}>{this.props.book.FullName}</li>
        )
    }
}
export default BookNavItem;