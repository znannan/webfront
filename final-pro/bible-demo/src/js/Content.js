import React, { Component } from 'react';
import ChapterNav from './ChapterNav';
import Verses from './Verses';
import { Typography } from 'antd';
class Content extends Component{
    constructor(props){
        super(props);
        this.state = {
            chapter:1
        }
    }

    changeChapter(chapter){
        this.setState({
            chapter: chapter
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps.currentBook && prevProps.currentBook !== this.props.currentBook){
            this.changeChapter(1);
        }
    }

    get currentBookName(){
        return this.props.currentBook && this.props.currentBook.FullName;
    }

    get chapters(){
        return this.props.currentBook && this.props.currentBook.ChapterNumber;
    }

    

    render(){
        return(
            // <main>
            //     <header>{this.currentBookName} {this.state.chapter} 章</header>
            //     <ChapterNav chapters={this.chapters} changeChapter={this.changeChapter.bind(this)} />
            //     <Verses currentBook={this.props.currentBook} currentChapter={this.state.chapter} />
            // </main>
            <main>
                <div className="stick-top">
                    <Typography.Title level={4} style={{textAlign:'center'}}>
                        {this.currentBookName} {this.state.chapter}章
                    </Typography.Title>
                    <ChapterNav chapters={this.chapters} currentChapter={this.state.chapter}
                      changeChapter={this.changeChapter.bind(this)} />
                </div>
                <Verses currentBook={this.props.currentBook} currentChapter={this.state.chapter} /> 
            </main>
        )
    }
}
export default Content;