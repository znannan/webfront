/*1. write a function filter which accept two parameters, the first is a select function, the second is the array. It only selects those elements who meet the requirement by the select function.
*
*signature: filter(func[Function], arr:[Array]) => Array
*function name: filter
*params: func -> function, which accept two params, element and index
*        array -> array
*return: array
*
*e.g.
*filter(function(el, index){
*    return el % 2 == 0;
*}, [1,2,3,4,5,6]) // should return [2,4,6]
*
*filter(function(el, index){
*    return el.length > 3;
*}, ['a','ab','abc','abcd','abcde','abcdef']) 
*should return ['abcd','abcde','abcdef']
*/
function filter (func, array){
    var i, len,newArray;
    len = array.length;
    newArray = [];
    for (i = 0; i < len; i++) {
        if(func(array[i], i)){
            newArray.push(array[i]);
        }
    }
    return newArray;
}
/*2. write a funciton called multiplyAll which takes an array of integers as an argument. 
*This function must return another function, which takes a single integer as an argument and returns a new array.
*The returned array should consist of each of the elements from the first array multiplied by the integer.
*
*signature: multiplyAll(arr[Array]) => Function
*function name: multiplyAll
*params: arr, Array type
*return: another function
*
*e.g.
*multiplyAll([1, 2, 3])(2) // should return  [2, 4, 6];
*/
function multiplyAll (arr){
    return function (b){
        return arr.map(function(e,i){
            return e*b;
        });
    } 
}
/*3.This is a simple exercise to understand the feature in the javascript language called closure.
*
*The function buildFun will return an array of functions. 
*The single parameter accepted by buildFun is the number of elements N of the array returned.
*The wanted outcome is that when all function in the array are executed, the number from 0 to N should be returned.
*
*The code below doesn't work quite right, please fix it.
*
*code: 
*function buildFun(n){
*    var res = [];
*    for (var i = 0; i< n; i++){
*        res.push(function(){
*            console.log(i);
*        })
*    }
*    return res;
*}
*/
function buildFun(n){
    var res = [];
    for (var i = 0; i<= n; i++){
        res.push((function(j){
            console.log(j);
        })(i));
    }
    return res;
}

/*4. You'll implement once, a function that takes another function as an argument, 
*and returns a new version of that function that can only be called once.
*signature: once(func[Function]) => Function
*function name: once
*params: func, which is a function
*
*e.g
*function add(a, b){
*    return a + b;
*}
*var addOnce = once(add);
*addOnce(5, 6); // return 11;
*addOnce(1, 3); // nothing happens, since the addOnce could only work once.
*
*
*/
function once (func){
    var init = 0;
    return function (){
        if(init==0){
            init += 1;
            return func.apply({},arguments);//
        }
    }
}
function add(a, b){
    return a + b;
}
var addOnce = once(add);
addOnce(5, 6);
addOnce(1, 3);