//use function 
function Queue(){
    this.que = [];
}

Queue.prototype.enqueue = function enqueue(arg){
    this.que.push(arg);
}

Queue.prototype.dequeue = function dequeue(){
    return this.que.shift();
}

Queue.prototype.forEach = function forEach(func){
    this.que.map(func)
}

Queue.prototype.length = function length(){
    return this.que.length;
}

Queue.prototype.isEmpty = function isEmpty(){
    return (this.que.length == 0);
}

var queue = new Queue();
queue.enqueue(1);
queue.enqueue(3.14);
queue.enqueue('a');
queue.forEach(function(ele){
    console.log(ele)
});
queue.length(); // it should return 3

var b = queue.dequeue();
console.log(b); // it should print out 1

queue.length(); // it should return 2
queue.isEmpty(); // it should return false

queue.dequeue();
queue.dequeue();

queue.isEmpty(); // it should return ture

/*use ES6 class */
class Queue2{
    constructor(){
      this.que = [];
    }
    
    enqueue(arg){
      this.que.push(arg);
    }

    dequeue(){
      return this.que.shift();
    }

    length(){
      return this.que.length;
    }

    isEmpty(){
      return (this.que.length==0);
    }

    forEach(func){
      this.que.map(func);
    }
}
var queue2 = new Queue2();
queue2.enqueue(1);
queue2.enqueue(3.14);
queue2.enqueue('a');
queue2.forEach(function(ele){
    console.log(ele)
});
queue2.length(); // it should return 3

var b2 = queue2.dequeue();
console.log(b2); // it should print out 1

queue2.length(); // it should return 2
queue2.isEmpty(); // it should return false

queue2.dequeue();
queue2.dequeue();

queue2.isEmpty(); // it should return ture