document.onreadystatechange = function(){//initial
  if(document.readyState==='complete'){
    listenDoneUndoTask();
    listenCreateNewTask();
  }
}
function listenDoneUndoTask(){//监听checkbox
  const ul = document.getElementById("todolist_ul");
  ul.addEventListener('click',(event) => {
    let ele = event.target;
    let todoText = ele.parentElement;
    if (ele.type=='checkbox' && ele.checked){
      todoText.classList.remove('undo');
      todoText.classList.add('done');
    }else if(ele.type=='checkbox' && !ele.checked){
      todoText.classList.remove('done');
      todoText.classList.add('undo');
    }
  });
}

function listenCreateNewTask(){//监听 submit
  const submit = document.getElementById('submit_button');
  submit.addEventListener('click',createNewTask);
}

function createNewTask(){ //添加一行li元素 作为new todo
  var new_todo = document.getElementById('new_todo');
  new_todo.value!=''?(()=>{
    let li = document.createElement('li');

    let label = document.createElement('label');
    label.classList.add('undo');

    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';

    label.append(checkbox);
    label.append(new_todo.value);

    li.append(label);
    //li.innerHTML = `<label class='undo'><input type="checkbox">$new_toto</label>`;
    const ul = document.getElementById("todolist_ul");
    ul.appendChild(li);

    new_todo.value='';
  })():alert('Empty content!');
}
