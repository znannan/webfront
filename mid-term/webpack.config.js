const path  = require('path');
const simpleServer = require('./simple-server.js');
module.exports = {
  target: "web",
  mode: "development",
  devtool: 'inline-source-map',
  entry: {
    app: './src/app.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  devServer: {
    watchContentBase: true,
    compress: true,
    open: true,
    port: 8009,
    writeToDisk: true,
    watchOptions: {
      poll: true
    },
    before(app, server){
      simpleServer.toDosHandler(app, server);
    }
  }
};

