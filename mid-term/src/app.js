document.onreadystatechange = function(){//initial
  if(document.readyState==='complete'){
    listenDoneUndoDeleteTask();
    listenCreateNewTask();
    getTodoList();
  }
}
function listenDoneUndoDeleteTask(){//监听checkbox_done_undo & delete
  const ul = document.getElementById("todolist_ul");
  ul.addEventListener('click',(event) => {
    let ele = event.target;
    //let todoText = ele.parentElement;
    if (ele.type=='checkbox' && ele.checked){
      // todoText.classList.remove('undo');
      // todoText.classList.add('done');
      updateTodoServer(ele.id,'done')
    }else if(ele.type=='checkbox' && !ele.checked){
      // todoText.classList.remove('done');
      // todoText.classList.add('undo');
      updateTodoServer(ele.id,'notdone')

    }else if(ele.title=='delete'){
      confirm("Delete, Sure?")?deleteTodoServer(ele.id):void(0);
    }
  });
}

function listenCreateNewTask(){//监听 submit
  const submit = document.getElementById('submit_button');
  submit.addEventListener('click',createNewTaskServer);
}

function createNewTaskEle(todoText, todoId, todoDone){ //添加一行li元素 作为new todo

    let li = document.createElement('li');

    let label = document.createElement('label');
    if(todoDone){
      label.classList.add('done');
    }else{
      label.classList.add('undo');
    }
    label.setAttribute("id", todoId);

    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    if(todoDone){
      checkbox.checked=true;
    }else{
      checkbox.checked=false;
    }
    checkbox.setAttribute("id", todoId);
    
    

    label.append(checkbox);
    label.append(todoText);

    let span_delete = document.createElement('span');
    span_delete.title = 'delete'
    span_delete.innerText = 'X'
    span_delete.setAttribute("id", todoId);

    li.append(label);
    li.append(span_delete);

    return li
  
}

function createNewTaskServer(){ //发送 new todo 到服务器
  var new_todo = document.getElementById('new_todo');
  new_todo.value!=''?(()=>{
    sendNewTodoServer(new_todo.value); // 发送服务器
    
  })():alert('Empty content!');
}

function deleteOldTask(oldTaskLi){ //直接在页面删todo_li,程序没有用到
  const ul = document.getElementById("todolist_ul");
  ul.removeChild(oldTaskLi);
}
/* 
 *以下为 ajax
*/
function getTodoList(){ //查
  const xhr = new XMLHttpRequest(),
        method = 'GET',
        url = '/to-do-lists';
  xhr.open(method, url);
  xhr.onreadystatechange = function(){
    if (xhr.readyState === 4 && xhr.status === 200){
      const todos = JSON.parse(xhr.response);
      const todoList = document.querySelector('#todolist_ul');
      todoList.innerHTML = '';
      for(let todo of todos.todos){
        const new_li = createNewTaskEle(todo.content,todo.id,todo.done);
        todoList.appendChild(new_li);
      }
    }
  }
  xhr.send();
}

function sendNewTodoServer(todoText){ //增
  const xhr = new XMLHttpRequest(),
        method = 'POST',
        url = '/to-do-lists';
  xhr.open(method, url);
  xhr.onreadystatechange = function(){
    if (xhr.readyState === 4 && xhr.status === 200){//发送成功之后
      new_todo.value=''; //清空textarea
      getTodoList(); //从服务器load回来todoLists 更新页面
    }
  }
  xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
  xhr.send(`todoItem=${todoText}`);

}

function deleteTodoServer(id){ //删
  const xhr = new XMLHttpRequest(),
        method = 'DELETE',
        url = `/to-do-lists/${id}`;
  xhr.open(method, url);
  xhr.onreadystatechange = function(){
    if (xhr.readyState === 4 && xhr.status === 200){//发送成功之后
      
      getTodoList(); //从服务器load回来todoLists 更新页面
    }
  }
  xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
  xhr.send();
}

function updateTodoServer(id,done_undo){//改
  const xhr = new XMLHttpRequest(),
        method = 'PUT',
        url = `/to-do-lists/${id}/${done_undo}`;
  xhr.open(method, url);
  xhr.onreadystatechange = function(){
    if (xhr.readyState === 4 && xhr.status === 200){//发送成功之后
      
      getTodoList(); //从服务器load回来todoLists 更新页面
    }
  }
  xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
  xhr.send();

}